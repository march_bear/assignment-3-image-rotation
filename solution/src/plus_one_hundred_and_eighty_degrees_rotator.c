#include <stdint.h>

#include "image_interface.h"

static uint64_t get_index_of_rotated_image(uint64_t i, uint64_t j, uint64_t i_height, uint64_t i_width) {
    return i_width * (i_height - i - 1) + (i_width - j - 1);
}

struct image rotate_plus_one_hundred_and_eighty_degrees(struct image const source) {
    uint64_t height = source.height;
    uint64_t width = source.width;

    return image_create_by(source, height, width, get_index_of_rotated_image);
}
