#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "cast_to_int16_t.h"

bool check_for_int16_t(char const * const s) {
    bool with_a_sign = false;
    char const * curr = s;
    uint8_t length = 0;
    uint8_t leading_zeros_counter = 0;
    while (*curr != '\0') {
        switch (*curr) {
            case '+':
            case '-':
                if (with_a_sign) return false;
                with_a_sign = true;
                break;
            case '0':
                if (length == 0) {
                    ++leading_zeros_counter;
                    break;
                }
                ++length;
                break;
            default:
                if (*curr >= '1' && *curr <= '9')
                    ++length;
                else
                    return false;
        }
        if (length == 7 || length + leading_zeros_counter > 16) {
            return false;
        }
        ++curr;
    }

    return true;
}

int16_t_wrapper get_int16_t_from_string(char const * const s) {
    if(!check_for_int16_t(s))
        return (int16_t_wrapper) { .correct = false };

    int int_result_value = atoi(s);
    int16_t result_value = (int16_t) int_result_value;
    if (int_result_value != result_value)
        return (int16_t_wrapper) { .correct = false };

    return (int16_t_wrapper) { .correct = true, .value = result_value};
}
