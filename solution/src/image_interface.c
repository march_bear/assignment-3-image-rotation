#include <stdint.h>
#include <stdlib.h>

#include "image.h"

struct image image_create_by(
    struct image const source,
    uint64_t height,
    uint64_t width,
    uint64_t (* index)(uint64_t i, uint64_t j, uint64_t i_height, uint64_t i_width)
) {
    struct image copy = {
        .height = height,
        .width = width,
        .data = NULL
    };

    copy.data = malloc(copy.height * copy.width * sizeof(struct pixel));

    for (uint64_t i = 0; i < copy.height; ++i) {
        uint64_t real_i_index = i * copy.width;
        for (uint64_t j = 0; j < copy.width; ++j) {
            copy.data[real_i_index + j] = source.data[index(i, j, source.height, source.width)];
        }
    }

    return copy;
}

static uint64_t get_real_index(uint64_t i, uint64_t j, uint64_t i_height, uint64_t i_width) {
    (void) i_height;
    return i * i_width + j;
}

struct image image_copy(struct image const source) {
    return image_create_by(source, source.height, source.width, get_real_index);
}

void image_clean(struct image source) {
    if (source.data != NULL)
        free(source.data);
}
