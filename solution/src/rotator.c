#include "image.h"
#include "r_angle.h"
#include "rotators.h"

typedef struct image (*rotator)(struct image const source);

static rotator const rotators[] = {
    [ANGLE_ZERO_DEGREES] = rotate_zero_degrees,
    [ANGLE_PLUS_NINETY_DEGREES] = rotate_plus_ninety_degrees,
    [ANGLE_PLUS_ONE_HUNDRED_AND_EIGHTY_DEGREES] = rotate_plus_one_hundred_and_eighty_degrees,
    [ANGLE_PLUS_TWO_HUNDRED_AND_SEVENTY_DEGREES] = rotate_plus_two_hundred_and_seventy_degrees,
    [ANGLE_MINUS_NINETY_DEGREES] = rotate_plus_two_hundred_and_seventy_degrees,
    [ANGLE_MINUS_ONE_HUNDRED_AND_EIGHTY_DEGREES] = rotate_plus_one_hundred_and_eighty_degrees,
    [ANGLE_MINUS_TWO_HUNDRED_AND_SEVENTY_DEGREES] = rotate_plus_ninety_degrees,
};

struct image rotate(struct image const source, enum r_angle const rotation_angle) {
    return rotators[rotation_angle](source);
}
