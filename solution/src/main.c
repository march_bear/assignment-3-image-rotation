#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp_io_with_messages.h"
#include "cast_to_int16_t.h"
#include "displayer.h"
#include "image_interface.h"
#include "r_angle_interface.h"
#include "rotator.h"

bool check_arg_count(int argc) {
    return argc == 4;
}

int main( int argc, char** argv ) {
    display_message_without_line_break("Checking and reading arguments... ");
    if (!check_arg_count(argc)) {
        display_error("Checking error: expected 3 arguments");
        exit(1);
    }

    const char * input_file_path = argv[1];
    const char * output_file_path = argv[2];

    int16_t_wrapper int16_t_r_angle = get_int16_t_from_string(argv[3]);
    if (!int16_t_r_angle.correct) {
        display_error("Checking error: the passed angle value is not a number or it is too long");
        exit(2);
    }

    r_angle_wrapper angle_wrapper = get_r_angle_from_int16_t(int16_t_r_angle.value);
    if (!angle_wrapper.correct) {
        display_error("Checking error: the number passed is not a valid angle value");
        display_error("Note: expected 0, 90, 180, 270, -90, -180 or -270");
        exit(3);
    }

    display_message("Done");

    FILE * input_file = fopen(input_file_path, "rb");

    struct image img = {0};

    display_message_without_line_break("Reading input file... ");

    enum read_status reading_result = from_bmp(input_file, &img);

    if (fclose(input_file) != 0) {
        display_error("Warning: input file was not successfully closed");
    }

    if (reading_result != READ_OK) {
        display_error(read_errors_messages[reading_result]);
        image_clean(img);
        exit(4);
    }

    display_message(read_errors_messages[reading_result]);

    FILE * output_file = fopen(output_file_path, "wb");
    struct image rotated_img = rotate(img, angle_wrapper.value);

    image_clean(img);

    display_message_without_line_break("Writing to output file... ");
    enum write_status writing_result = to_bmp(output_file, &rotated_img);

    image_clean(rotated_img);

    if (fclose(output_file) != 0) {
        display_error("IOError: output file was not successfully closed. Exit...");
        exit(5);
    }

    if (writing_result != WRITE_OK) {
        display_error(write_errors_messages[writing_result]);
        exit(6);
    }

    display_message(write_errors_messages[writing_result]);

    display_message("Image rotation is performed");

    return 0;
}
