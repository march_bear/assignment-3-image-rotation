#include <stdint.h>

#include "image_interface.h"

static uint64_t get_index_of_rotated_image(uint64_t i, uint64_t j, uint64_t i_height, uint64_t i_width) {
    (void) i_height;
    return i_width * (i_height - j - 1) + i;
}

struct image rotate_plus_ninety_degrees(struct image const source) {
    uint64_t height = source.width;
    uint64_t width = source.height;

    return image_create_by(source, height, width, get_index_of_rotated_image);
}
