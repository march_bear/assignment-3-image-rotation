#include <stdbool.h>
#include <stdint.h>

#include "r_angle.h"

r_angle_wrapper get_r_angle_from_int16_t(int16_t angle) {
    r_angle_wrapper result = { .correct = true };
    switch (angle) {
        case 0:
            result.value = ANGLE_ZERO_DEGREES;
            break;
        case 90:
            result.value = ANGLE_PLUS_NINETY_DEGREES;
            break;
        case -90:
            result.value = ANGLE_MINUS_NINETY_DEGREES;
            break;
        case 180:
            result.value = ANGLE_PLUS_ONE_HUNDRED_AND_EIGHTY_DEGREES;
            break;
        case -180:
            result.value = ANGLE_MINUS_ONE_HUNDRED_AND_EIGHTY_DEGREES;
            break;
        case 270:
            result.value = ANGLE_PLUS_TWO_HUNDRED_AND_SEVENTY_DEGREES;
            break;
        case -270:
            result.value = ANGLE_MINUS_TWO_HUNDRED_AND_SEVENTY_DEGREES;
            break;
        default:
            result.correct = false;
    }

    return result;
}
