#ifndef ROTATORS_H
#define ROTATORS_H

#include "image.h"

struct image rotate_zero_degrees(struct image const source);
struct image rotate_plus_ninety_degrees(struct image const source);
struct image rotate_plus_one_hundred_and_eighty_degrees(struct image const source);
struct image rotate_plus_two_hundred_and_seventy_degrees(struct image const source);

#endif
