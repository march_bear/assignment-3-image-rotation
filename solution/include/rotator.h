#ifndef ROTATOR_H
#define ROTATOR_H

#include <stdint.h>

#include "image.h"
#include "r_angle.h"

struct image rotate(struct image const source, enum r_angle const rotation_angle);

#endif
