#ifndef BMP_IO_H
#define BMP_IO_H

#include <stdio.h>

#include "image.h"

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_COMPRESSION,
    READ_INVALID_FILE_POINTER,
    READ_INVALID_IMAGE,
};

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_ERROR,
    WRITE_INVALID_FILE_POINTER
};

enum write_status to_bmp(FILE* out, struct image const* img);

#endif
