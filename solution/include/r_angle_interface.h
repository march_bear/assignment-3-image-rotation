#ifndef R_ANGLE_INTERFACE_H
#define R_ANGLE_INTERFACE_H

#include <stdint.h>

#include "r_angle.h"

r_angle_wrapper get_r_angle_from_int16_t(int16_t angle);

#endif
