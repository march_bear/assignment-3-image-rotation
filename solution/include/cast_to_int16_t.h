#ifndef CAST_TO_INT16_T_H
#define CAST_TO_INT16_T_H

typedef struct {
    _Bool correct;
    int16_t value;
} int16_t_wrapper;

int16_t_wrapper get_int16_t_from_string(char const * const s);

#endif
