#ifndef R_ANGLE_H
#define R_ANGLE_H

enum r_angle {
    ANGLE_ZERO_DEGREES = 0,
    ANGLE_PLUS_NINETY_DEGREES,
    ANGLE_PLUS_ONE_HUNDRED_AND_EIGHTY_DEGREES,
    ANGLE_PLUS_TWO_HUNDRED_AND_SEVENTY_DEGREES,
    ANGLE_MINUS_NINETY_DEGREES,
    ANGLE_MINUS_ONE_HUNDRED_AND_EIGHTY_DEGREES,
    ANGLE_MINUS_TWO_HUNDRED_AND_SEVENTY_DEGREES,
};

typedef struct {
    _Bool correct;
    enum r_angle value;
} r_angle_wrapper;

#endif
