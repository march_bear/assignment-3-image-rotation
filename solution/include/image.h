#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

#define PIXEL_SIZE 3

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif
