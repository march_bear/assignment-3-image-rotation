#ifndef IMAGE_INTERFACE_H
#define IMAGE_INTERFACE_H

#include <stdint.h>

#include "image.h"

struct image image_copy(struct image const source);

struct image image_create_by(
    struct image source,
    uint64_t height,
    uint64_t width,
    uint64_t (* index)(uint64_t i, uint64_t j, uint64_t i_height, uint64_t i_width)
);

void image_clean(struct image source);

#endif
